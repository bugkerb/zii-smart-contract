# Zii Smart Contracts

This repository contains Zii smart contract project.

This project is intended to be used with the
[OpenZeppelin Learn](https://docs.openzeppelin.com/learn/) and 
[Hardhat Beginners Tutorial](https://hardhat.org/tutorial), but you should be
able to follow it by yourself by reading the README and exploring its
`contracts`, `tests`, `scripts` directories.

## Quick start

The first things you need to do are cloning this repository and installing its
dependencies:

```sh
git clone https://gitlab.wisden.io/wisden/zii/smart-contracts.git
```
or
```sh
git clone git@gitlab.wisden.io:wisden/zii/smart-contracts.git
```

Create .env file then add environment config (copy from .env.sample)
```sh
PRIVATE_KEY=--- Enter Private Key ---
SELLER_ADDR=--- Enter Seller Address ---
```

and then

```sh
cd smart-contracts
npm install
```

Once installed, let's run Hardhat's testing network:

```sh
npx hardhat node
```

Then, on a new terminal, go to the repository's root folder and run this to
deploy your contract:

```sh
npx hardhat run scripts/deploy.js --network localhost
```

Finally, we can run test with:

```sh
npx hardhat test
```

**Enjoy _building_!**

// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Snapshot.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/security/Pausable.sol";

contract Zii is ERC20, ERC20Snapshot, AccessControl, Pausable {
    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");
    bytes32 public constant SELLER_ROLE = keccak256("SELLER_ROLE");
    address private _seller;
    bool private _pausable = true;

    event SellerTransferred(address indexed oldSeller, address indexed newSeller);

    /**
     * @dev Modifier to make a function callable only when the contract is not paused.
     *
     * Requirements:
     *
     * - The contract must not be paused except for the role of the admin and seller.
     */
    modifier whenNotPausedExceptAdminAndSeller() {
        require(!paused() || hasRole(DEFAULT_ADMIN_ROLE, _msgSender()) || hasRole(SELLER_ROLE, _msgSender()), "Pausable: paused");
        _;
    }

    /**
     * @dev Initialize Contract
     *
     * - set seller address 
     * - grant role `Admin` to owner
     * - grant role `Pauser` to owner
     * - grant role `Seller` to seller address
     * - mint 400,000,000 tokens to seller *** No one can use this function anymore ***
     * - pause transfer until public sale period
     *
     * Requirements:
     *
     * - seller address 
     */
    constructor(address sellerAccount) ERC20("Zii", "Zii") {
        _seller = sellerAccount;
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(PAUSER_ROLE, msg.sender);
        _grantSeller();
        _mint(_seller, 400000000 ether);
        _pause();
    }

    /**
     * @dev Returns seller address.
     *
     */
    function seller() external view returns (address) {
        return _seller;
    }

    /**
     * @dev Grants `role` to `account`.
     *
     * Internal function without access restriction.
     *
     * Requirements:
     *
     * - `role` must not be `seller`.
     * - `account` cannot be the zero address.
     */
    function _grantRole(bytes32 role, address account) internal override(AccessControl)
    {
        require(role != SELLER_ROLE, "AccessControl: cannot grant seller role directly");
        super._grantRole(role, account);
    }

    /**
     * @dev Grants `seller role` to `account`.
     *
     * Private function without access restriction.
     *
     */
    function _grantSeller() private {
        super._grantRole(SELLER_ROLE, _seller);
    }

    /**
     * @dev Transfer seller role and tokens to the new address.
     *
     * Requirements:
     *
     * - The role must be `admin`.
     *
     * [WARNING]
     * ====
     * Please recheck your new seller address before using this function
     */
    function transferSeller(address newSeller) external onlyRole(DEFAULT_ADMIN_ROLE) {
        address oldSeller = _seller;
        super._grantRole(SELLER_ROLE, newSeller);
        _revokeRole(SELLER_ROLE, oldSeller);
        _transfer(oldSeller, newSeller, balanceOf(oldSeller));
        _seller = newSeller;
        emit SellerTransferred(oldSeller, newSeller);
    }

    /**
     * @dev Snapshot contract.
     *
     * Requirements:
     *
     * - The role must be `admin`.
     */
    function snapshot() external onlyRole(DEFAULT_ADMIN_ROLE) {
        _snapshot();
    }

    /**
     * @dev Triggers stopped state.
     *
     * Requirements:
     *
     * - The contract must not be paused and pausable.
     */
    function pause() external onlyRole(PAUSER_ROLE) {
        require(_pausable, "Pausable: disabled");
        _pause();
    }

    /**
     * @dev Returns to a normal state and disables the pause feature.
     *
     * Requirements:
     *
     * - The contract must be paused.
     */
    function unpause() external onlyRole(PAUSER_ROLE) {
        _unpause();
        _pausable = false;
    }

    /**
     * @dev Returns pausable state.
     *
     */
    function pausable() external view returns (bool) {
        return _pausable;
    }

    /**
     * @dev Destroys `amount` tokens from `account`, reducing the
     * total supply.
     *
     * Emits a {Transfer} event with `to` set to the zero address.
     *
     * Requirements:
     *
     * - `account` cannot be the zero address.
     * - `account` must have at least `amount` tokens.
     */
    function burn(address account, uint256 amount) external onlyRole(DEFAULT_ADMIN_ROLE) returns (bool) {
        _burn(account, amount);
        return true;
    }

    /**
     * @dev Do bulk transfers in one transaction.
     *
     * Requirements:
     *
     * - The role must be `seller`.
     */
    function bulkTransfer(address[] calldata _receivers, uint256[] calldata _amounts) external onlyRole(SELLER_ROLE) {
		require(_receivers.length == _amounts.length, "Zii: address number != amount number");
		for (uint256 i = 0; i < _receivers.length; i++) {
			_transfer(msg.sender, _receivers[i], _amounts[i]);
		}
	}

    /**
     * @dev Check `whenNotPausedExceptAdminAndSeller` before transfer token
     *
     * Internal function without access restriction.
     *
     */
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal whenNotPausedExceptAdminAndSeller override(ERC20, ERC20Snapshot)
    {
        super._beforeTokenTransfer(from, to, amount);
    }
}
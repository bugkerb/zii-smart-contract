const { config } = require("chai");

// scripts/deploy.js
async function main () {
  const Config = require('../config.js');
  const config = new Config();
  // We get the contract to deploy
  const Token = await ethers.getContractFactory('Zii');
  console.log('Deploying Token...');
  const seller = config.seller_address;
  const token = await Token.deploy(seller);
  await token.deployed();
  console.log('Token deployed to:', token.address);

  // deploy miner auction
  const Auction = await ethers.getContractFactory('MinerAuction');
  console.log('Deploying Auction...');
  const auction = await Auction.deploy(token.address, 200);
  await auction.deployed();
  console.log('Auction deployed to:', auction.address);

  // unpause transfer
  const unpause = await token.unpause();
  console.log('Unpaused');
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
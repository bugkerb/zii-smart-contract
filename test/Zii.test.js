// test/Zii.test.js
// Load dependencies
const { expect } = require('chai');

// Import utilities from Test Helpers
const { BN, constants, ether, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

// Load compiled artifacts
const Zii = artifacts.require('Zii');

// Start test block
contract('Zii', function ([ admin, pauser, seller, user1, user2, user3 ]) {

  const amount = new ether('10')
  const ADMIN_ROLE = constants.ZERO_BYTES32
  const PAUSER_ROLE = web3.utils.keccak256('PAUSER_ROLE')
  const SELLER_ROLE = web3.utils.keccak256('SELLER_ROLE')
  
  beforeEach(async function () {
    this.token = await Zii.new(seller, { from: admin });
  
    // grant pauser role to pauser
    await this.token.grantRole(PAUSER_ROLE, pauser, {from: admin})  

    // transfer token from seller to user1
    await this.token.transfer(user1, amount, { from: seller })
  });

  describe('Token Info', function() {
    // Test case
    it('get correct name', async function () {
      // Check token name
      expect((await this.token.name()).toString()).to.equal('Zii');
    });

    it('get correct symbol', async function () {
      // Check token symbol
      expect((await this.token.symbol()).toString()).to.equal('Zii');
    });

    it('get correct decimals', async function () {
      // Check token symbol
      expect((await this.token.decimals()).toString()).to.equal('18');
    });

    it('get total supply 400000000 Tokens', async function () {
      // Check token symbol
      expect((await this.token.totalSupply())).to.be.bignumber.equal(ether('400000000'));
    });

    it('get correct pausable state', async function () {
      // Check pausable state
      expect((await this.token.pausable())).to.equal(true);
    });

    it('get correct seller address', async function () {
      // Check seller address
      expect((await this.token.seller()).toString()).to.equal(seller);
    });
  });

  describe('Grant Role', function() {

    it('can grant admin role by admin', async function () {
      // grant role admin to user1 by admin
      const grant = await this.token.grantRole(ADMIN_ROLE, user1, {from: admin})
      // check role admin on user1
      expect(await this.token.hasRole(ADMIN_ROLE, user1)).to.equal(true)
      // check emit RoleGranted event
      expectEvent(grant, 'RoleGranted', {
        role: ADMIN_ROLE,
        account: user1
      })
    });

    it('can grant pauser role by admin', async function () {
      // grant role pauser to user1 by admin
      const grant = await this.token.grantRole(PAUSER_ROLE, user1, {from: admin})
      // check role pauser on user1
      expect(await this.token.hasRole(PAUSER_ROLE, user1)).to.equal(true)
      // check emit RoleGranted event
      expectEvent(grant, 'RoleGranted', {
        role: PAUSER_ROLE,
        account: user1
      })
    });

    it('cannot grant admin role by non admin', async function () {
      await expectRevert(
        this.token.grantRole(ADMIN_ROLE, admin, {from: user1}),
        `AccessControl: account ${user1.toLowerCase()} is missing role ${ADMIN_ROLE}`,
      );
    });

    it('cannot grant pauser role by non admin', async function () {
      await expectRevert(
        this.token.grantRole(PAUSER_ROLE, pauser, {from: user1}),
        `AccessControl: account ${user1.toLowerCase()} is missing role ${ADMIN_ROLE}`,
      );
    });

    it('cannot grant seller role directly', async function () {
      await expectRevert(
        this.token.grantRole(SELLER_ROLE, seller, {from: admin}),
        'AccessControl: cannot grant seller role directly',
      );
    });
  });

  describe('Revoke Role', function() {

    it('can revoke admin role by admin', async function () {
      // revoke role admin by admin
      const revoke = await this.token.revokeRole(ADMIN_ROLE, admin, {from: admin})
      // check role admin
      expect(await this.token.hasRole(ADMIN_ROLE, admin)).to.equal(false)
      // check emit RoleRevoked event
      expectEvent(revoke, 'RoleRevoked', {
        role: ADMIN_ROLE,
        account: admin
      })
    });

    it('can revoke pauser role by admin', async function () {
      // revoke role pauser by admin
      const revoke = await this.token.revokeRole(PAUSER_ROLE, pauser, {from: admin})
      // check role pauser
      expect(await this.token.hasRole(PAUSER_ROLE, pauser)).to.equal(false)
      // check emit RoleRevoked event
      expectEvent(revoke, 'RoleRevoked', {
        role: PAUSER_ROLE,
        account: pauser
      })
    });

    it('can revoke seller role by admin', async function () {
      // revoke role seller by admin
      const revoke = await this.token.revokeRole(SELLER_ROLE, seller, {from: admin})
      // check role seller
      expect(await this.token.hasRole(SELLER_ROLE, seller)).to.equal(false)
      // check emit RoleRevoked event
      expectEvent(revoke, 'RoleRevoked', {
        role: SELLER_ROLE,
        account: seller
      })
    });

    it('cannot revoke admin role by non admin', async function () {
      await expectRevert(
        this.token.revokeRole(ADMIN_ROLE, admin, {from: user1}),
        `AccessControl: account ${user1.toLowerCase()} is missing role ${ADMIN_ROLE}`,
      );
    });

    it('cannot revoke pauser role by non admin', async function () {
      await expectRevert(
        this.token.revokeRole(PAUSER_ROLE, pauser, {from: user1}),
        `AccessControl: account ${user1.toLowerCase()} is missing role ${ADMIN_ROLE}`,
      );
    });

    it('cannot revoke seller role by non admin', async function () {
      await expectRevert(
        this.token.revokeRole(SELLER_ROLE, seller, {from: user1}),
        `AccessControl: account ${user1.toLowerCase()} is missing role ${ADMIN_ROLE}`,
      );
    });
  });

  describe('Renounce Role', function() {
    it('can renounce admin role by admin', async function () {
      // renounce role admin by admin
      const renounce = await this.token.renounceRole(ADMIN_ROLE, admin, {from: admin})
      // check role admin
      expect(await this.token.hasRole(ADMIN_ROLE, admin)).to.equal(false)
      // check emit RoleRevoked event
      expectEvent(renounce, 'RoleRevoked', {
        role: ADMIN_ROLE,
        account: admin
      })
    });

    it('can renounce pauser role by pauser', async function () {
      // renounce role admin by admin
      const renounce = await this.token.renounceRole(PAUSER_ROLE, pauser, {from: pauser})
      // check role pauser
      expect(await this.token.hasRole(PAUSER_ROLE, pauser)).to.equal(false)
      // check emit RoleRevoked event
      expectEvent(renounce, 'RoleRevoked', {
        role: PAUSER_ROLE,
        account: pauser
      })
    });

    it('can renounce seller role by seller', async function () {
      // renounce role seller by seller
      const renounce = await this.token.renounceRole(SELLER_ROLE, seller, {from: seller})
      // check role seller
      expect(await this.token.hasRole(SELLER_ROLE, seller)).to.equal(false)
      // check emit RoleRevoked event
      expectEvent(renounce, 'RoleRevoked', {
        role: SELLER_ROLE,
        account: seller
      })
    });

    it('cannot renounce admin role by non admin', async function () {
      await expectRevert(
        this.token.renounceRole(ADMIN_ROLE, admin, {from: pauser}),
        'AccessControl: can only renounce roles for self',
      );
    });

    it('cannot renounce pauser role by non pauser', async function () {
      await expectRevert(
        this.token.renounceRole(PAUSER_ROLE, pauser, {from: admin}),
        'AccessControl: can only renounce roles for self',
      );
    });

    it('cannot renounce seller role by non seller', async function () {
      await expectRevert(
        this.token.renounceRole(SELLER_ROLE, seller, {from: admin}),
        'AccessControl: can only renounce roles for self',
      );
    });
  });

  describe('Transfer Seller', function() {
    it('can transfer seller by admin', async function () {
      // get balance of seller
      const balanceOfSeller = await this.token.balanceOf(seller)
      // get balance of user1
      const balanceOfUser1 = await this.token.balanceOf(user1)
      // transfer seller by admin
      const transferSeller = await this.token.transferSeller(user1, {from: admin})
      // check current seller
      expect(await this.token.seller()).to.equal(user1)
      // summary balance
      const totalBalance = balanceOfSeller.add(balanceOfUser1)
      // check balance of old seller
      expect(await this.token.balanceOf(seller)).to.be.bignumber.equal('0');
      // check balance of new seller
      expect(await this.token.balanceOf(user1)).to.be.bignumber.equal(totalBalance);

      // check emit SellerTransferred event
      expectEvent(transferSeller, 'SellerTransferred', {
        oldSeller: seller,
        newSeller: user1
      })
    });

    it('cannot transfer seller by non admin', async function () {
      // Test transfer seller by non admin
      await expectRevert(
        this.token.transferSeller(seller, { from: seller }),
        `AccessControl: account ${seller.toLowerCase()} is missing role ${ADMIN_ROLE}`,
      );
    });
  });

  describe('Allowance', function() {
    it('can allow other addresses to be spenders with the maximum amount', async function () {
      // user1 allows spending by user2 
      const approve = await this.token.approve(user2, constants.MAX_UINT256, {from: user1})
      // check allowance amount
      expect(await this.token.allowance(user1, user2)).to.be.bignumber.equal(constants.MAX_UINT256)
      // check emit Approval event
      expectEvent(approve, 'Approval', {
        owner: user1,
        spender: user2,
        value: constants.MAX_UINT256
      })
    });

    it('can allow other addresses to be spenders with specific amount', async function () {
      // get current amount of allowance
      const currentAllowance = await this.token.allowance(user1, user2)
      // user1 allows spending by user2 
      const approve = await this.token.approve(user2, amount, {from: user1})
      // summary amount
      const totalAllowance = amount.add(currentAllowance)
      // check allowance amount
      expect(await this.token.allowance(user1, user2)).to.be.bignumber.equal(totalAllowance)
      // check emit Approval event
      expectEvent(approve, 'Approval', {
        owner: user1,
        spender: user2,
        value: totalAllowance
      })
    });

    it('can increase allowance to spencers', async function () {
      // get current amount of allowance
      const currentAllowance = await this.token.allowance(user1, user2)
      // user1 increase allowance to user2
      const allowance = await this.token.increaseAllowance(user2, amount, {from: user1})
      // summary amount
      const totalAllowance = amount.add(currentAllowance)
      // check allowance amount
      expect(await this.token.allowance(user1, user2)).to.be.bignumber.equal(totalAllowance)
      // check emit Approval event
      expectEvent(allowance, 'Approval', {
        owner: user1,
        spender: user2,
        value: totalAllowance
      })
    });

    it('can decrease allowance to spencers', async function () {
      // user1 allows spending by user2 
      await this.token.approve(user2, amount, {from: user1})
      // get current amount of allowance
      const currentAllowance = await this.token.allowance(user1, user2)
      // user1 increase allowance to user2
      const allowance = await this.token.decreaseAllowance(user2, amount, {from: user1})
      // summary amount
      const totalAllowance = currentAllowance.sub(amount)
      // check allowance amount
      expect(await this.token.allowance(user1, user2)).to.be.bignumber.equal(totalAllowance)
      // check emit Approval event
      expectEvent(allowance, 'Approval', {
        owner: user1,
        spender: user2,
        value: totalAllowance
      })
    });

    it('cannot increase allowance over max uint256', async function () {
      // user1 allows spending by user2 
      await this.token.approve(user2, amount, {from: user1})
      // user1 increase allowance to user2
      await expectRevert.unspecified(
        this.token.increaseAllowance(user2, constants.MAX_UINT256, {from: user1})
      );
    });

    it('cannot decrease allowance below zero', async function () {
      // user1 allows spending by user2 
      await this.token.approve(user2, amount, {from: user1})
      // user1 decrease allowance to user2
      await expectRevert(
        this.token.decreaseAllowance(user2, constants.MAX_UINT256, {from: user1}),
        'ERC20: decreased allowance below zero'
      );
    });

    it('cannot use zero addresses to be spenders', async function () {
      // user2 allows spending by zero address
      await expectRevert(
        this.token.approve(constants.ZERO_ADDRESS, constants.MAX_UINT256, {from: user2}),
        'ERC20: approve to the zero address',
      )
    });
  })

  describe('Pausable', function() {

    it('can unpause by pauser', async function () {
      // unpause
      const unpause = await this.token.unpause({ from: pauser })
      // check paused
      expect(await this.token.paused()).to.equal(false);
      // check emit unpaused event
      expectEvent(unpause, 'Unpaused')
      // check pausable status
      expect(await this.token.pausable()).to.equal(false);
    });

    it('cannot pause/unpause by non pauser', async function () {
      // Test pause by non pauser
      await expectRevert(
        this.token.pause({ from: seller }),
        `AccessControl: account ${seller.toLowerCase()} is missing role ${PAUSER_ROLE}`,
      );
      
      // Test unpause by non pauser
      await expectRevert(
        this.token.unpause({ from: seller }),
        `AccessControl: account ${seller.toLowerCase()} is missing role ${PAUSER_ROLE}`,
      );
    });

    it('cannot pause again after unpaused', async function () {
      // unpause
      await this.token.unpause({ from: pauser })
 
      // Test pause by pauser
      await expectRevert(
        this.token.pause({ from: pauser }),
        'Pausable: disabled',
      );
    });
  });

  describe('Transfer Token', function() {

    it('can transfer tokens that are owned', async function () {
      // unpause 
      const unpause = await this.token.unpause({ from: pauser})
      // check emit paused event
      expectEvent(unpause, 'Unpaused')

      // get balance user2
      const balanceOfUser2 = await this.token.balanceOf(user2)
      // transfer token from user1 to user2
      const transfer = await this.token.transfer(user2, amount, { from: user1 })
      // summary balance
      const totalBalance = amount.add(balanceOfUser2)
      // check balance
      expect(await this.token.balanceOf(user2)).to.be.bignumber.equal(totalBalance);
      // check emit Transfer event
      expectEvent(transfer, 'Transfer', {
        from: user1,
        to: user2,
        value: amount
      })
    });

    it('can transfer tokens that are not owned but allowanced', async function () {
      // unpause 
      const unpause = await this.token.unpause({ from: pauser})
      // check emit paused event
      expectEvent(unpause, 'Unpaused')
      
      // user1 allows spending by user2 
      const approve = await this.token.approve(user2, amount, {from: user1})
      // check emit Approval event
      expectEvent(approve, 'Approval', {
        owner: user1,
        spender: user2,
        value: amount
      })
      // get current amount of allowance
      const currentAllowance = await this.token.allowance(user1, user2)
      
      // get balance user1
      const balanceOfUser1 = await this.token.balanceOf(user1)
      // transfer token from user1 to user2 by user2
      const transfer = await this.token.transferFrom(user1, user2, amount, { from: user2 })
      // summary balance
      const totalBalance = balanceOfUser1.sub(amount)
      // check balance
      expect(await this.token.balanceOf(user1)).to.be.bignumber.equal(totalBalance);
      // check emit Transfer event
      expectEvent(transfer, 'Transfer', {
        from: user1,
        to: user2,
        value: amount
      })

      // check emit Approval event
      expectEvent(transfer, 'Approval', {
        owner: user1,
        spender: user2,
        value: currentAllowance.sub(amount)
      })
    });

    it('can transfer tokens that by seller when pause', async function () {
      // get balance user1
      const balanceOfUser1 = await this.token.balanceOf(user1)
      // transfer token from seller to user1
      const transfer = await this.token.transfer(user1, amount, { from: seller })
      // summary balance
      const totalBalance = amount.add(balanceOfUser1)
      // check balance
      expect(await this.token.balanceOf(user1)).to.be.bignumber.equal(totalBalance);
      // check emit Transfer event
      expectEvent(transfer, 'Transfer', {
        from: seller,
        to: user1,
        value: amount
      })
    });

    it('cannot transfer tokens that amount exceeds the balance', async function () {
      // unpause 
      const unpause = await this.token.unpause({ from: pauser})
      // check emit paused event
      expectEvent(unpause, 'Unpaused')

      // transfer token from user1 to user2
      const transferAmount = amount.add(new ether('10'))
      await expectRevert(
        this.token.transfer(user2, transferAmount, { from: user1 }),
        'ERC20: transfer amount exceeds balance',
      );
      
    });

    it('cannot transfer tokens that are not owned and insufficient allowance', async function () {
      // unpause 
      const unpause = await this.token.unpause({ from: pauser})
      // check emit paused event
      expectEvent(unpause, 'Unpaused')
      
      // user1 allows spending by user2 
      const approve = await this.token.approve(user2, amount, {from: user1})
      // check emit Approval event
      expectEvent(approve, 'Approval', {
        owner: user1,
        spender: user2,
        value: amount
      })

      // transfer token from user1 by user2
      const transferAmount = amount.add(new ether('10'))
      await expectRevert(
        this.token.transferFrom(user1, user2, transferAmount, { from: user2 }),
        'ERC20: insufficient allowance',
      );
      
    });

    it('cannot transfer token that owned to zero address', async function () {
      // unpause 
      const unpause = await this.token.unpause({ from: pauser})
      // check emit paused event
      expectEvent(unpause, 'Unpaused')
      
      // transfer token from user1 to zero address
      await expectRevert(
        this.token.transfer(constants.ZERO_ADDRESS, amount, { from: user1 }),
        'ERC20: transfer to the zero address',
      );
    });

    it('cannot transfer token that not owned but allowanced to zero address', async function () {
      // unpause 
      const unpause = await this.token.unpause({ from: pauser})
      // check emit paused event
      expectEvent(unpause, 'Unpaused')

      // user1 allows spending by user2 
      const approve = await this.token.approve(user2, amount, {from: user1})
      // check emit Approval event
      expectEvent(approve, 'Approval', {
        owner: user1,
        spender: user2,
        value: amount
      })
      
      // transfer token from user1 to zero address by user2
      await expectRevert(
        this.token.transferFrom(user1, constants.ZERO_ADDRESS, amount, { from: user2 }),
        'ERC20: transfer to the zero address',
      );
    });

    it('cannot transfer token when paused', async function () {
      // transfer token from user1 to user2
      await expectRevert(
        this.token.transfer(user2, amount, { from: user1 }),
        'Pausable: paused',
      );
    });
  });

  describe('Bulk Transfer Token', function() {

    it('can bulk transfer tokens', async function () {
      // unpause 
      const unpause = await this.token.unpause({ from: pauser})
      // check emit paused event
      expectEvent(unpause, 'Unpaused')

      const amount2 = amount.add(ether('10'))

      // get balance seller
      const balanceOfSeller = await this.token.balanceOf(seller)
      // get balance user2
      const balanceOfUser2 = await this.token.balanceOf(user2)
      // get balance user3
      const balanceOfUser3 = await this.token.balanceOf(user3)
      // bulk transfer token from user1 to user2 and user3
      const transfer = await this.token.bulkTransfer([user2, user3], [amount, amount2], { from: seller })
      // summary balance
      const totalBalanceSeller = balanceOfSeller.sub(amount.add(amount2))
      const totalBalanceUser2 = amount.add(balanceOfUser2)
      const totalBalanceUser3 = amount2.add(balanceOfUser3)
      // check balance
      expect(await this.token.balanceOf(seller)).to.be.bignumber.equal(totalBalanceSeller);
      expect(await this.token.balanceOf(user2)).to.be.bignumber.equal(totalBalanceUser2);
      expect(await this.token.balanceOf(user3)).to.be.bignumber.equal(totalBalanceUser3);
      // check emit Transfer event
      expectEvent(transfer, 'Transfer', {
        from: seller,
        to: user2,
        value: amount
      })
      expectEvent(transfer, 'Transfer', {
        from: seller,
        to: user3,
        value: amount2
      })

      // console.log(`gasUsed: ${transfer.receipt.gasUsed}`)
    });

    it('can bulk transfer tokens by seller when pause', async function () {
      const amount2 = amount.add(ether('10'))
      // get balances
      const balanceOfUser1 = await this.token.balanceOf(user1)
      const balanceOfUser2 = await this.token.balanceOf(user2)
      // bulk transfer token from seller to user1, user2
      const transfer = await this.token.bulkTransfer([user1,user2], [amount,amount2], { from: seller })
      // summary balance
      const totalBalance = amount.add(balanceOfUser1)
      const totalBalance2 = amount2.add(balanceOfUser2)
      // check balance
      expect(await this.token.balanceOf(user1)).to.be.bignumber.equal(totalBalance);
      expect(await this.token.balanceOf(user2)).to.be.bignumber.equal(totalBalance2);
      // check emit Transfer event
      expectEvent(transfer, 'Transfer', {
        from: seller,
        to: user1,
        value: amount
      })
      expectEvent(transfer, 'Transfer', {
        from: seller,
        to: user2,
        value: amount2
      })
    });

    it('cannot bulk transfer tokens that amount exceeds the balance', async function () {
      // unpause 
      const unpause = await this.token.unpause({ from: pauser})
      // check emit paused event
      expectEvent(unpause, 'Unpaused')

      // bulk transfer token from user1 to [user2, user3]
      const transferAmount = amount.add(new ether('250000000'))
      await expectRevert(
        this.token.bulkTransfer([user2, user3], [transferAmount, transferAmount], { from: seller }),
        'ERC20: transfer amount exceeds balance',
      );
      
    });

    it('cannot bulk transfer token to zero address', async function () {
      // unpause 
      const unpause = await this.token.unpause({ from: pauser})
      // check emit paused event
      expectEvent(unpause, 'Unpaused')
      
      // transfer token from user1 to user2 and zero address
      await expectRevert(
        this.token.bulkTransfer([user2, constants.ZERO_ADDRESS], [amount, amount], { from: seller }),
        'ERC20: transfer to the zero address',
      );
    });

    it('cannot bulk transfer token by not seller', async function () {
      // unpause 
      const unpause = await this.token.unpause({ from: pauser})
      // check emit paused event
      expectEvent(unpause, 'Unpaused')
      
      // transfer token from user1 to user2 and zero address
      await expectRevert(
        this.token.bulkTransfer([user2, constants.ZERO_ADDRESS], [amount, amount], { from: user1 }),
        `AccessControl: account ${user1.toLowerCase()} is missing role ${SELLER_ROLE}`,
      );
    });
  });
});
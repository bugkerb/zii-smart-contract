/**
 * @type import('hardhat/config').HardhatUserConfig
 */

require('@nomiclabs/hardhat-ethers');
require("@nomiclabs/hardhat-truffle5");

const Config = require('./config.js');
const config = new Config();
//const privateKey = config.private_key
module.exports = {
  solidity: "0.8.4",
  defaultNetwork: "hardhat",
  networks: {
    hardhat: {
      chainId: 31337
    },
    // testnet: {
    //   url: "https://data-seed-prebsc-1-s1.binance.org:8545",
    //   chainId: 97,
    //   gasPrice: 50000000000,
    //   // accounts: {
    //   //   mnemonic: mnemonic
    //   // }
    //   accounts: [privateKey]
    // },
    // mainnet: {
    //   url: "https://bsc-dataseed.binance.org/",
    //   chainId: 56,
    //   gasPrice: 5000000000,
    //   // accounts: {
    //   //   mnemonic: mnemonic
    //   // }
    //   accounts: [privateKey]
    // }
  },
};

require('dotenv').config();
class Config {
  constructor() {
    // private key of deployer wallet
    this.private_key = process.env["PRIVATE_KEY"] || null; 

    // seller address
    this.seller_address = process.env["SELLER_ADDR"] || null; 

  }
}

module.exports = Config;